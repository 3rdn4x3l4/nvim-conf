local ls = require("luasnip") --{{{
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {} --}}}

local function create_snippet(trigger, nodes, opts) --{{{
	local snippet = s(trigger, nodes)
	local target_table = snippets

	if opts ~= nil then
		-- if opts is a string
		if type(opts) == "string" then
			if opts == "auto" then
				target_table = autosnippets
			end
		end
	end

	table.insert(target_table, snippet) -- insert snippet into appropriate table
end
-- args
local getChoiceArgs = function(idx)
	return c(idx, {
		sn(1, fmt([[{}]], { i(1, "arg1") })),
		sn(2, fmt([[{}, {}]], { i(1, "arg1"), i(2, "arg2"), })),
		sn(3, fmt([[{}, {}, {}]], { i(1, "arg1"), i(2, "arg2"), i(3, "arg3"), })),
		sn(4, fmt([[{}, {}, {}, {}]], { i(1, "arg1"), i(2, "arg2"), i(3, "arg3"), i(4, "arg4"), })),
	})
end


create_snippet( -- {regexSnippet} LuaSnippet{{{
	"regexSnippet",
	fmt(
		[=[
cs( -- {}
{{ trig = "{}", regTrig = true, hidden = true }}, fmt([[ 
{}
]], {{
  {}
}}))
      ]=],
		{
			i(1, "Description"),
			i(2, ""),
			i(3, ""),
			i(4, ""),
		}
	)
) --}}}

create_snippet(
	"function",
	fmt(
		[[ 
function {}({})
  {}
end
]],
		{
			i(1, "function_name"),
			getChoiceArgs(2),
			i(3, "-- body"),
		}
	)
)

create_snippet(
	"local_function",
	fmt(
		[[ 
local function {}({})
  {}
end
]],
		{
			i(1, "function_name"),
			getChoiceArgs(2),
			i(3, "-- body"),
		}
	)
)

create_snippet(
	"local",
	fmt(
		[[ 
local {} = {}
  ]],
		{ i(1, ""), i(2, "") }
	)
)

-- create_snippet(
-- 	{
-- 		trig = "for([%w_]+)",
-- 		regTrig = true,
-- 		hidden = true
-- 	},
-- 	fmt([[
-- 		for {} in {} do
-- 		  {}
-- 		end
-- 		]],
-- 		{
-- 			d(1, function(_, snip)
-- 				return sn(1, i(1, snip.captures[1]))
-- 			end),
-- 			i(2, "iterated"),
-- 			i(3, "// TODO:")
-- 		}
-- 	),
-- 	"auto"
-- )

return snippets, autosnippets
