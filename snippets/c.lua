local ls = require("luasnip") --{{{
local s = ls.s --> snippet
local i = ls.i --> insert node
local t = ls.t --> text node

local d = ls.dynamic_node
local c = ls.choice_node
local f = ls.function_node
local sn = ls.snippet_node

local fmt = require("luasnip.extras.fmt").fmt
local rep = require("luasnip.extras").rep

local snippets, autosnippets = {}, {} --}}}

local function create_snippet(trigger, nodes, opts) --{{{
	local snippet = s(trigger, nodes)
	local target_table = snippets

	if opts ~= nil then
		-- if opts is a string
		if type(opts) == "string" then
			if opts == "auto" then
				target_table = autosnippets
			end
		end
	end

	table.insert(target_table, snippet) -- insert snippet into appropriate table
end

local getChoiceArgs = function(idx)
	return c(idx, {
		sn(1, fmt([[{}]], { i(1, "arg1") })),
		sn(2, fmt([[{}, {}]], { i(1, "arg1"), i(2, "arg2"), })),
		sn(3, fmt([[{}, {}, {}]], { i(1, "arg1"), i(2, "arg2"), i(3, "arg3"), })),
		sn(4, fmt([[{}, {}, {}, {}]], { i(1, "arg1"), i(2, "arg2"), i(3, "arg3"), i(4, "arg4"), })),
	})
end

-- function
create_snippet("statfunc",
fmt([[ 
static {} {}({})
{{
  {}
}}
]],
{
	i(1, "void"),
	i(2, "function_name"),
	getChoiceArgs(3),
	i(4, "//body"),
})
)

-- function
create_snippet("func",
fmt([[ 
{}	{}({})
{{
  {}
}}
]], {
	i(1, "void"),
	i(2, "function_name"),
	getChoiceArgs(3),
	i(4, "//body"),
})
)

-- while
create_snippet(
	"while",
	fmt(
		[[ 
while ({})
{{
  {}
}}
]],
		{
			i(1, "condition"),
			i(2, "//body"),
		}
	)
)
-- if
create_snippet(
	"if",
	fmt(
		[[ 
if ({})
{{
  {}
}}
]],
		{
			i(1, "test"),
			i(2, "//body"),
		}
	)
)
-- elseif
create_snippet(
	"elseif",
	fmt(
		[[ 
else if ({})
{{
  {}
}}
]],
		{
			i(1, "test"),
			i(2, "//body"),
		}
	)
)
-- else
create_snippet(
	"else",
	fmt(
		[[ 
else
{{
  {}
}}
]],
		{
			i(1, "//body"),
		}
	)
)

return snippets, autosnippets
