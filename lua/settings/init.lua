local options = {
    termguicolors = true,
    mouse = "a",
    syntax = "on",
    autoindent = true,
    autoread = true,
    swapfile = false,
    ruler = true,
    number = true,
    relativenumber = true,
    cursorline = true,
    hlsearch = false,
    incsearch = true,
    smartcase = true,
    hidden = true,
    path = ".,./**",
    wrap = false,
    tabstop = 4,
    shiftwidth = 4,
    softtabstop = 4,
    expandtab = false,
    inccommand = 'split',
    wildmenu = true,
    wildmode = {"longest","list","full" },
	ignorecase = true,
    showtabline = 2,
    equalalways = true,
    splitright = true,
    splitbelow = true,
    eadirection = "both",
    scrolloff = 999,
    sidescrolloff = 5,
    foldenable = true,
    foldlevelstart = 9,
    foldmethod = "manual",
    winbar = "%t %m #%n %y", -- [[ filename, state, bufNbr, cursor pos in % of file ]] I want to add a command to query timestamp
    showcmd = true,
    cmdheight = 1,
    spell = true,
    updatetime = 50,
    --textwidth = 80,
    colorcolumn = "80",
    background = "dark",
    ls = 3,
    timeoutlen = 300,
	virtualedit = "block",
	clipboard = "unnamedplus",
	completeopt = "menu,menuone,noinsert",
}

vim.opt.shortmess:append "c"
vim.g.mapleader = " "
vim.g.maplocalleader = ";"

for k, v in pairs(options) do
  vim.opt[k] = v
end
