function create_command(...)
	vim.api.nvim_create_user_command(...)
end

function getPathOfCurrentFile()
	return vim.fn.expand("%:p:h")
end

local attach_to_buffer = function(output_bufnbr, pattern, command)
	vim.api.nvim_create_autocmd({"BufWritePost"}, {
		group = vim.api.nvim_create_augroup(pattern, {clear = true}),
		pattern = pattern,
		callback = function()
			local append_data = function(_, data)
				if data then
					vim.api.nvim_buf_set_lines(output_bufnbr, -1, -1, false, data)
				end
			end

			vim.api.nvim_buf_set_lines(output_bufnbr, 0, -1, false, { command .. " output:" })
			vim.fn.jobstart(command, {
				on_stderr = append_data,
				on_stdout = append_data
			})
		end
	})
end

local auto_run = function()
	-- Param file at root of repo
	-- Result file(s) in subroot of repo
	-- Result filename with Pattern command


	local pattern = vim.fn.input("Pattern: ")
	if pattern == nil or pattern == '' then
		pattern = vim.fn.expand('%')
	end

	local command = vim.fn.input("Command: ")

	local path = '' -- root of repo
	local filename = path .. "." .. pattern .. command .. ".txt"

	local new_buff_command = "vnew " .. filename

	vim.cmd(new_buff_command)
	local bufnr = tonumber(vim.api.nvim_get_current_buf())
	attach_to_buffer(bufnr, pattern, command)

end
create_command("AutoRun", auto_run, {})
