local opts = {
	default_path = "~/.tmp/"
}
require('f_seashell').setup(opts)
vim.keymap.set('n', '<leader>zsh', require"f_seashell".command_prompt, { silent = true, noremap = true, desc = 'shell++'})
