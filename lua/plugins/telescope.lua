return {
	-- Telescope extension

	-- Telescope
	{
		"nvim-telescope/telescope.nvim",
		dependencies = {
			"nvim-lua/popup.nvim",
			"nvim-lua/plenary.nvim",
			{ "nvim-telescope/telescope-file-browser.nvim" },
			{ 'nvim-telescope/telescope-fzf-native.nvim',  build = 'make' },
			{ "nvim-telescope/telescope-ui-select.nvim" },
			{ "nvim-telescope/telescope-dap.nvim" },
		},
		config = function()
			local telescope = require('telescope')
			local actions = require('telescope.actions')
			local builtin = require('telescope.builtin')
			local previewers = require('telescope.previewers')
			local extensions = telescope.extensions
			local pickers = require "telescope.pickers"
			local finders = require "telescope.finders"
			local make_entry = require "telescope.make_entry"

			local function flash(prompt_bufnr)
				require("flash").jump({
					pattern = "^",
					label = { after = { 0, 0 } },
					search = {
						mode = "search",
						exclude = {
							function(win)
								return vim.bo[vim.api.nvim_win_get_buf(win)].filetype ~= "TelescopeResults"
							end,
						},
					},
					action = function(match)
						local picker = require("telescope.actions.state").get_current_picker(prompt_bufnr)
						picker:set_selection(match.pos[1] - 1)
					end,
				})
			end

			telescope.setup {
				defaults = {

					vimgrep_arguments = {
						'rg',
						'--hidden',
						'--color=never',
						'--no-heading',
						'--with-filename',
						'--line-number',
						'--column',
						'--smart-case',
					},

					prompt_prefix = "$>",
					selection_strategy = "reset",
					sorting_strategy = "ascending",
					layout_strategy = "bottom_pane",
					layout_config = {
						height = {
							padding = 0
						},
						width = {
							padding = 0
						}
					},
					prompt_position = "top",
					border = false,
					winblend = 0,
					color_devicons = true,
					use_less = true,
					set_env = { ['COLORTERM'] = 'truecolor' },

					file_ignore_patterns = { "node_modules/", ".git/", ".mypy_cache/" },

					file_previewer = previewers.vim_buffer_cat.new,
					grep_previewer = previewers.vim_buffer_vimgrep.new,
					qflist_previewer = previewers.vim_buffer_qflist.new,

					mappings = {
						i = {
							-- ['<c-s>'] = actions.select_horizontal,
							['<c-space>'] = actions.toggle_selection + actions.move_selection_next,
							['<c-f>'] = actions.to_fuzzy_refine,
							["<c-s>"] = flash
						},
						n = {
							s = flash
						}
					},

					cache_picker = {
						num_pickers = -1,
						limit_entries = 1000
					}
				},
				pickers = {
					buffers = {
						mappings = {
							i = {
								['<c-x>'] = actions.delete_buffer
							}
						},
					},
					current_buffer_fuzzy_find = {
						skip_empty_lines = true
					},
					man_pages = {
						sections = { "ALL" }
					},
					find_files = {
						hidden = true,
						no_ignore = true,
						no_ignore_parent = true
					},
					git_files = {
						show_untracked = true
					},
					builtin = {
						include_extensions = true
					}
				},
				extensions = {
					file_browser = {
						hijack_netrw = true,
						cwd_to_path = true,
						hidden = true,
						-- respect_gitignore = true
					},
				}
			}
			telescope.load_extension('fzf')
			telescope.load_extension('file_browser')
			telescope.load_extension('dap')
			telescope.load_extension('ui-select')
			telescope.load_extension('notify')
			-- telescope.load_extension("refactoring")

			local M = {}
			-- this line should be always after setup
			local conf = require "telescope.config".values


			M.multigrep = function(opts)
				opts = opts or {}
				opts.cwd = opts.cwd or vim.uv.cwd()

				local finder = finders.new_async_job {
					command_generator = function(prompt)
						if not prompt or prompt == "" then
							return nil
						end

						local pieces = vim.split(prompt, "  ")
						local args = { "rg" }
						if pieces[1] then
							table.insert(args, "-e")
							table.insert(args, pieces[1])
						end
						if pieces[2] then
							table.insert(args, "-g")
							table.insert(args, pieces[2])
						end
						return vim.tbl_flatten {
							args,
							'--hidden',
							'--follow',
							'--color=never',
							'--no-heading',
							'--with-filename',
							'--line-number',
							'--column',
							'--smart-case',
						}
					end,
					entry_maker = make_entry.gen_from_vimgrep(opts),
					cwd = opts.cwd
				}

				pickers.new(opts, {
					debounce = 100,
					prompt_title = "MultiGrep",
					finder = finder,
					previewer = conf.grep_previewer(opts),
					sorter = require("telescope.sorters").empty(),
				}):find()
			end

			M.search_plugins = function()
				builtin.find_files({
					prompt_title = "< Plugins >",
					-- cwd = "~/dotfiles/Common/nvim/.config/nvim"
					cwd = vim.fs.joinpath(vim.fn.stdpath('data'), "lazy")
				})
			end

			M.search_vimconfig = function()
				builtin.git_files({
					prompt_title = "< VIM CONF >",
					-- cwd = "~/dotfiles/Common/nvim/.config/nvim"
					cwd = vim.fn.stdpath('config')
				})
			end

			M.search_dotfiles = function()
				builtin.git_files({
					prompt_title = "< DOTFILES >",
					cwd = "~/dotfiles/"
				})
			end

			M.search_config = function()
				builtin.git_files({
					prompt_title = "< CONFIG >",
					cwd = "~/config"
				})
			end

			local telescopts = {
				silent = true, -- use `silent` when creating keymaps
				noremap = true, -- use `noremap` when creating keymaps
			}

			telescopts.desc = 'vim plugins'
			vim.keymap.set('n', '<leader>plug', M.search_plugins, telescopts)
			telescopts.desc = 'vim config'
			vim.keymap.set('n', '<leader>vim', M.search_vimconfig, telescopts)
			telescopts.desc = 'dotfiles'
			vim.keymap.set('n', '<leader>dot', M.search_dotfiles, telescopts)
			telescopts.desc = 'config'
			vim.keymap.set('n', '<leader>cfg', M.search_config, telescopts)
			telescopts.desc = 'git files'
			vim.keymap.set('n', '<C-g>', function() builtin.git_files({ cwd = getPathOfCurrentFile() }) end, telescopts)
			telescopts.desc = 'find files'
			vim.keymap.set('n', '<C-f>', function() builtin.find_files({ cwd = getPathOfCurrentFile() }) end, telescopts)
			telescopts.desc = 'notifications'
			vim.keymap.set('n', '<leader>not', extensions.notify.notify, telescopts)
			telescopts.desc = 'telescope register'
			vim.keymap.set('n', '<leader>reg', builtin.registers, telescopts)
			-- telescopts.desc = 'registers'
			-- vim.keymap.set('i', '<c-r>', builtin.registers, telescopts)
			telescopts.desc = 'file browser'
			vim.keymap.set('n', '<leader>fb',
				function() extensions.file_browser.file_browser({ cwd = getPathOfCurrentFile() }) end, telescopts)
			telescopts.desc = 'man pages'
			vim.keymap.set('n', '<leader>man', builtin.man_pages, telescopts)
			telescopts.desc = 'buffer fuzzy'
			vim.keymap.set('n', '<leader>/', builtin.current_buffer_fuzzy_find, telescopts)
			telescopts.desc = 'telescope builtins'
			vim.keymap.set('n', '<leader>btn', builtin.builtin, telescopts)
			telescopts.desc = 'telescope pickers'
			vim.keymap.set('n', '<leader>pks', builtin.pickers, telescopts)
			telescopts.desc = 'telescope resume'
			vim.keymap.set('n', '<leader>rsm', builtin.resume, telescopts)
			telescopts.desc = 'open buffers'
			vim.keymap.set('n', '<leader>bu', builtin.buffers, telescopts)
			telescopts.desc = 'oldfiles'
			vim.keymap.set('n', '<leader>of', builtin.oldfiles, telescopts)
			telescopts.desc = 'commands'
			vim.keymap.set('n', '<leader>cmd', builtin.commands, telescopts)
			telescopts.desc = 'commands history'
			vim.keymap.set('n', '<leader>his', builtin.command_history, telescopts)
			telescopts.desc = 'help tags'
			vim.keymap.set('n', '<leader>gh', builtin.help_tags, telescopts)
			telescopts.desc = 'quickfix'
			vim.keymap.set('n', '<leader>qfl', builtin.quickfix, telescopts)
			telescopts.desc = 'quickfixhistory'
			vim.keymap.set('n', '<leader>qfh', builtin.quickfixhistory, telescopts)
			telescopts.desc = 'filetypes'
			vim.keymap.set('n', '<leader>ftp', builtin.filetypes, telescopts)
			-- telescopts.desc = 'spell suggest'
			-- vim.keymap.set('n', '<leader>spl', builtin.spell_suggest, telescopts)
			-- telescopts.desc = 'refactoring'
			-- vim.keymap.set({'n', 'v'}, "<leader>rr", extensions.refactoring.refactors, telescopts)
			telescopts.desc = 'find keymaps'
			vim.keymap.set('n', '<leader>map', builtin.keymaps, telescopts)
			telescopts.desc = 'show dap commands'
			vim.keymap.set('n', '<leader>dcmd', extensions.dap.commands, telescopts)
			telescopts.desc = 'show dap configurations'
			vim.keymap.set('n', '<leader>dcfg', extensions.dap.configurations, telescopts)
			telescopts.desc = 'multi grep'
			vim.keymap.set('n', '<leader>mrg', M.multigrep, telescopts)
		end
	},

}
