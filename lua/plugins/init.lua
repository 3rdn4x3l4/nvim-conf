return {

	{
		-- notify
		"rcarriga/nvim-notify",
		config = function()
			local notify = require("notify")
			notify.setup({
				background_colour = "#000000",
				render = "minimal"
			})
			vim.notify = notify
		end
	},

	"nvim-lua/plenary.nvim",

	-- keymap autocmd
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		opts = {
			delay = 200
		},
	},
	{ "tpope/vim-obsession" },

	-- own register telescope picker
	-- {"plagache/f_seashell.nvim", branch = "test"},
	-- "/home/alex/doc/f_seashell.nvim"

	{
		"mechatroner/rainbow_csv",
		ft = "csv"
	},


	-- lsp support Mason
	{
		"williamboman/mason.nvim",
		build = function()
			require("mason").setup()
			vim.cmd("MasonUpdate")
		end,
		config = function()
			require("mason").setup()
		end,
	},

	{
		"stevearc/oil.nvim",
		-- opts = {},
		-- Optional dependencies
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("oil").setup({
				default_file_explorer = true,
				columns = {
					"icon",
					"size",
					"mtime",
					"permissions",
				},
				view_options = {
					show_hidden = true,
				},
				keymaps = {
					["<C-h>"] = false,
					["<C-l>"] = false,
					-- ["<C-v>"] = "actions.select_vsplit",
					["<C-s>"] = "actions.select_split",
					["<C-r>"] = "actions.refresh",
					["-"] = false,
					[".."] = "actions.parent",
				}
			})
			vim.keymap.set("n", '<leader>cd', require("oil").open,
				{ silent = true, noremap = true, desc = "Open parent directory" })
		end
	},


	-- Cheat Sheet
	"RishabhRD/popfix",
	{
		"RishabhRD/nvim-cheat.sh",
		config = function()
			vim.g.cheat_default_window_layout = 'split'
			vim.keymap.set('n', '<leader>cht', '<cmd>Cheat<CR>',
				{ silent = true, noremap = true, desc = 'open cheat query' })
		end
	},

	{
		"folke/flash.nvim",
		event = "VeryLazy",
		---@type Flash.Config
		opts = {
			search = {
				mode = "fuzzy"
			},
			label = {
				rainbow = {
					enabled = true,
					shade = 7
				}
			}
		},
		keys = {
			{ "s",     mode = { "n", "x", "o" }, function() require("flash").jump() end,              desc = "Flash" },
			{ "S",     mode = { "n", "x", "o" }, function() require("flash").treesitter_search() end, desc = "Flash Treesitter" },
			{ "r",     mode = "o",               function() require("flash").remote() end,            desc = "Remote Flash" },
			{ "<c-s>", mode = { "c" },           function() require("flash").toggle() end,            desc = "Toggle Flash Search" },
		},
	},
	{
		'MagicDuck/grug-far.nvim',
		event = "VeryLazy",
		config = function()
			require('grug-far').setup();
		end,
		keys = {
			{ "<leader>rg", mode = "n", function() require('grug-far').open() end,                                                     desc = "rg" },
			{ "<leader>*",  mode = "n", function() require('grug-far').open({ prefills = { search = vim.fn.expand("<cword>") } }) end, desc = "rg current word" },
		}
	},
}
