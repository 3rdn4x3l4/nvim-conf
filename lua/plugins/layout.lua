return {
	{ 
		event = "VeryLazy",
		"anuvyklack/windows.nvim",
		dependencies = {
			"anuvyklack/middleclass",
			"anuvyklack/animation.nvim"
		},
		config = function()
			vim.o.winwidth = 2
			vim.o.winminwidth = 2
			vim.o.equalalways = false

			local windows = require("windows")

			local opts = {
				autowidth = {
					enable = true,
					winwidth = 0.5,
					filetype = {
						help = 2,
					},
				},
				ignore = {
					buftype = { "quickfix", "dap-repl", "dapui_stacks", "dapui_scopes", "dapui_breakpoints", "dapui_watches" },
					filetype = { "NvimTree", "neo-tree", "undotree", "gundo", "toggleterm" }
				},
				animation = {
					enable = false,
				}
			}
			windows.setup(opts)
			vim.keymap.set('n', '<leader>mm', '<cmd>WindowsMaximize<CR>', { silent = true, noremap = true, desc = 'maximizer toogle'})
			vim.keymap.set('n', '<C-w>=', '<cmd>WindowsEqualize<CR>', { silent = true, noremap = true, desc = 'equalize windows'})
		end
	},
	{
		event = "VeryLazy",
		"xorid/swap-split.nvim",
		config = function()
			vim.keymap.set('n', '<leader>sws', require("swap-split").swap, {  silent = true, noremap = true, desc = 'swap split'})
		end
	},

	-- Icons
	{
		"nvim-lualine/lualine.nvim",
		dependencies = {"nvim-tree/nvim-web-devicons"},
		config = function()
			require('lualine').setup{
				options = {
					theme = 'gruvbox',
					section_separators = {'', ''},
					component_separators = {'|', '|'},
					icons_enabled = true,
					globalstatus = true,
				},
				sections = {
					lualine_a = { {'mode', upper = true} },
					lualine_b = { {'branch', icon = ''} },
					lualine_c = { {'filename', file_status = true} },
					lualine_x = { 'encoding', 'fileformat', 'filetype' },
					lualine_y = { 'progress' },
					lualine_z = { 'location'  },
				},
				inactive_sections = {
					lualine_a = {  },
					lualine_b = {  },
					lualine_c = { 'filename' },
					lualine_x = { 'location' },
					lualine_y = {  },
					lualine_z = {   }
				},
				-- extensions = { 'fzf' }
			}
		end
	},

}
