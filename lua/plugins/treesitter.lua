return {

	-- Treesitter
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		config = function()
			require 'nvim-treesitter.configs'.setup {

				ensure_installed = { "bash", "c", "css", "html", "json", "lua", "python", "vim", "yaml" },
				disable = function(lang, buf)
					local max_filesize = 100 * 1024 -- 100 KB
					local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
					if ok and stats and stats.size > max_filesize then
						return true
					end
				end,

				highlight = {
					enable = true,
				},
				indent = {
					enable = false
				},
				incremental_selection = {
					enable = true,
					keymaps = {
						node_incremental = "<leader>k",
						node_decremental = "<leader>j",
						scope_incremental = "<leader>h",
					},
				},
				-- autotag = {
				-- 	enable = true,
				-- },
				refactor = {
					highlight_definitions = { enable = true },
					highlight_current_scope = { enable = false },
					smart_rename = {
						enable = false,
						keymaps = {
							smart_rename = "<leader>rn",
						},
					},
				},

			}
		end
	},


	{
		"nvim-treesitter/playground",
		dependencies = {"nvim-treesitter/nvim-treesitter"}
	},

	-- Bracket
	{
		"HiPhish/rainbow-delimiters.nvim",
		dependencies = {"nvim-treesitter/nvim-treesitter"},
		config = function()
			require('rainbow-delimiters.setup').setup{}
		end
	},

	{
		"windwp/nvim-ts-autotag",
		dependencies = {"nvim-treesitter/nvim-treesitter"},
		config = {}
	},

	--Refactor
	{
		"nvim-treesitter/nvim-treesitter-refactor",
		dependencies = {"nvim-treesitter/nvim-treesitter"}
	},

	-- {
	-- 	"ThePrimeagen/refactoring.nvim",
	-- 	dependencies = {
	-- 		"nvim-lua/plenary.nvim",
	-- 		"nvim-treesitter/nvim-treesitter",
	-- 	},
	-- 	config = function()
	-- 		require("refactoring").setup()
	-- 	end,
	-- },

}
