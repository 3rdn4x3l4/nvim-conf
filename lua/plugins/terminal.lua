return {
	{
		"aserowy/tmux.nvim",
		config = function()
			local function termcodes(str)
				return vim.api.nvim_replace_termcodes(str, true, true, true)
			end

			local tmux = require("tmux")

			local opts = {
				silent = true,
				noremap = true,
			}
			opts.desc = 'escape terminal'
			vim.keymap.set({'t'}, '<esc>', termcodes('<C-\\><C-N>'), opts)
			opts.desc = 'resize'
			vim.keymap.set({'t', 'i'}, '<M-h>', tmux.resize_left, opts)
			opts.desc = 'resize'
			vim.keymap.set({'t', 'i'}, '<M-j>', tmux.resize_bottom, opts)
			opts.desc = 'resize'
			vim.keymap.set({'t', 'i'}, '<M-k>', tmux.resize_top, opts)
			opts.desc = 'resize'
			vim.keymap.set({'t', 'i'}, '<M-l>', tmux.resize_right, opts)
			opts.desc = 'move in terminal mode'
			vim.keymap.set({'t'}, '<C-h>', tmux.move_left, opts)
			opts.desc = 'move in terminal mode'
			vim.keymap.set({'t'}, '<C-j>', tmux.move_bottom, opts)
			opts.desc = 'move in terminal mode'
			vim.keymap.set({'t'}, '<C-k>', tmux.move_top, opts)
			opts.desc = 'move in terminal mode'
			vim.keymap.set({'t'}, '<C-l>', tmux.move_right, opts)
			-- insert Mode

			tmux.setup({
				copy_sync = {
					-- enables copy sync. by default, all registers are synchronized.
					-- to control which registers are synced, see the `sync_*` options.
					enable = true,

					-- ignore specific tmux buffers e.g. buffer0 = true to ignore the
					-- first buffer or named_buffer_name = true to ignore a named tmux
					-- buffer with name named_buffer_name :)
					ignore_buffers = { empty = false },

					-- TMUX >= 3.2: all yanks (and deletes) will get redirected to system
					-- clipboard by tmux
					redirect_to_clipboard = true,

					-- offset controls where register sync starts
					-- e.g. offset 2 lets registers 0 and 1 untouched
					register_offset = 0,

					-- overwrites vim.g.clipboard to redirect * and + to the system
					-- clipboard using tmux. If you sync your system clipboard without tmux,
					-- disable this option!
					sync_clipboard = true,

					-- synchronizes registers *, +, unnamed, and 0 till 9 with tmux buffers.
					sync_registers = true,

					-- synchronizes registers when pressing p and P.
					sync_registers_keymap_put = false,

					-- synchronizes registers when pressing (C-r) and ".
					sync_registers_keymap_reg = false,

					-- syncs deletes with tmux clipboard as well, it is adviced to
					-- do so. Nvim does not allow syncing registers 0 and 1 without
					-- overwriting the unnamed register. Thus, ddp would not be possible.
					sync_deletes = true,

					-- syncs the unnamed register with the first buffer entry from tmux.
					sync_unnamed = true,
				},
				navigation = {
					-- cycles to opposite pane while navigating into the border
					cycle_navigation = true,

					-- enables default keybindings (C-hjkl) for normal mode
					enable_default_keybindings = true,

					-- prevents unzoom tmux when navigating beyond vim border
					persist_zoom = false,
				},
				resize = {
					-- enables default keybindings (A-hjkl) for normal mode
					enable_default_keybindings = true,

					-- sets resize steps for x axis
					resize_step_x = 2,

					-- sets resize steps for y axis
					resize_step_y = 2,
				}
			})
		end
	},

	-- Terminal
	{
		event = "VeryLazy",
		"akinsho/toggleterm.nvim",
		-- version = "*",
		config = function()
			require("toggleterm").setup{
				size = 100,
				direction = 'float',
				hide_numbers = true, -- hide the number column in toggleterm buffers
				-- shade_filetypes = {},
				autochdir = true, -- when neovim changes it current directory the terminal will change it's own when next it's opened
				shade_terminals = true, -- NOTE: this option takes priority over highlights specified so if you specify Normal highlights you should set this to false
				shading_factor = 1, -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
				start_in_insert = true,
				insert_mappings = false, -- whether or not the open mapping applies in insert mode
				terminal_mappings = false, -- whether or not the open mapping applies in the opened terminals
				persist_size = false,
				persist_mode = true, -- if set to true (default) the previous terminal mode will be remembered
				close_on_exit = true, -- close the terminal window when the process exits
				shell = vim.o.shell, -- change the default shell
				auto_scroll = true, -- automatically scroll to the bottom on terminal output
				float_opts = {
					border = 'curved',
					width = vim.o.columns,
					height = vim.o.lines,
					winblend = 3,
				},
				winbar = {
					enabled = false,
					name_formatter = function(term) --  term: Terminal
						return term.name
					end
				},
			}

			local togtermopts = { noremap=true, silent=true}

			togtermopts.desc = 'toggle terminal'
			vim.keymap.set('n', '<leader>sh', '<cmd>ToggleTerm<CR>', togtermopts)
			togtermopts.desc = 'float terminal'
			vim.keymap.set('n', '<leader>fsh', '<cmd>ToggleTerm direction=float<CR>', togtermopts)
			togtermopts.desc = 'tab terminal'
			vim.keymap.set('n', '<leader>tsh', '<cmd>ToggleTerm direction=tab<CR>', togtermopts)
			togtermopts.desc = 'split terminal'
			vim.keymap.set('n', '<leader>vsh', '<cmd>ToggleTerm direction=vertical<CR>', togtermopts)
			togtermopts.desc = 'send line to terminal'
			vim.keymap.set('n', '<leader>lsh', '<cmd>ToggleTermSendCurrentLine<CR>', togtermopts)
			togtermopts.desc = 'send visual selection to terminal'
			vim.keymap.set('v', '<leader>vsh', '<cmd>ToggleTermSendVisualSelection<CR><esc>', togtermopts)
		end
	},
	{ 
		event = "VeryLazy",
		"chomosuke/term-edit.nvim",
		opts = {
			mapping = { 
				n = {
					s = false,
					S = false,
					r = false,
				}
			},
			-- Mandatory option:
			-- Set this to a lua pattern that would match the end of your prompt.
			-- Or a table of multiple lua patterns where at least one would match the
			-- end of your prompt at any given time.
			-- For most bash/zsh user this is '%$ '.
			-- For most powershell/fish user this is '> '.
			-- For most windows cmd user this is '>'.
			prompt_end = '%$>',
			-- How to write lua patterns: https://www.lua.org/pil/20.2.html
		},
		ft = 'toggleterm',
		version = "1.x"
	},

	{
		"willothy/flatten.nvim",
		dependencies = {
			"akinsho/toggleterm.nvim",
		},
		opts = {
			block_for = {
				gitcommit = true,
				gitrebase = true,
			},
			-- Command passthrough
			allow_cmd_passthrough = true,
			-- Allow a nested session to open if Neovim is opened without arguments
			nest_if_no_args = false,
			-- Window options
			window = {
				-- Options:
				-- current        -> open in current window (default)
				-- alternate      -> open in alternate window (recommended)
				-- tab            -> open in new tab
				-- split          -> open in split
				-- vsplit         -> open in vsplit
				-- smart          -> smart open (avoids special buffers)
				-- OpenHandler    -> allows you to handle file opening yourself (see Types)
				--
				open = "alternate",
				-- Options:
				-- vsplit         -> opens files in diff vsplits
				-- split          -> opens files in diff splits
				-- tab_vsplit     -> creates a new tabpage, and opens diff vsplits
				-- tab_split      -> creates a new tabpage, and opens diff splits
				-- OpenHandler    -> allows you to handle file opening yourself (see Types)
				diff = "tab_vsplit",
				-- Affects which file gets focused when opening multiple at once
				-- Options:
				-- "first"        -> open first file of new files (default)
				-- "last"         -> open last file of new files
				focus = "first",
			},
		},
		-- Ensure that it runs first to minimize delay when opening file from terminal
		lazy = false,
		priority = 1001,
	},
}
