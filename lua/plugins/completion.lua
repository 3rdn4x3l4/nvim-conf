return {
{
	"L3MON4D3/LuaSnip",
	event = "VeryLazy",
	config = function()
		local ls = require("luasnip")

		require("luasnip.loaders.from_lua").load({ paths = "~/.config/nvim/snippets/" })

		local config = { 
		}
		ls.config.setup(config)

		-- Virtual Text
		local types = require("luasnip.util.types")
		ls.config.set_config({
			history = true, --keep around last snippet local to jump back
			updateevents = "TextChanged,TextChangedI", --update changes as you type
			enable_autosnippets = true,
			ext_opts = {
				[types.choiceNode] = {
					active = {
						virt_text = {{"●", "GruvboxOrange"}}
					}
				},
				[types.insertNode] = {
					active = {
						virt_text = {{"●", "GruvboxBlue"}}
					}
				}
			},
		}) --

		-- Key Mapping

		local function jump_next()
			local ls = require("luasnip")
			if ls and ls.jumpable(1) then
				ls.jump(1)
			end
		end

		local function jump_previous()
			local ls = require("luasnip")
			if ls and ls.jumpable(-1) then
				ls.jump(-1)
			end
		end

		local function select_next()
			local ls = require("luasnip")
			if ls and ls.choice_active() then
				ls.change_choice(1)
			end
		end

		local function select_previous()
			local ls = require("luasnip")
			if ls and ls.choice_active() then
				ls.change_choice(-1)
			end
		end

		local opts = {silent = true}
		opts.desc = 'next choiceNode'
		vim.keymap.set({'i', 's'}, '<c-l>', select_next, opts)
		opts.desc = 'previous choiceNode'
		vim.keymap.set({'i', 's'}, '<c-h>', select_previous, opts)
		opts.desc = 'jump to next node'
		vim.keymap.set({'i', 's'}, '<c-j>', jump_next, opts)
		opts.desc = 'jump to previous node'
		vim.keymap.set({'i', 's'}, '<c-k>', jump_previous, opts)
	end
},

{
	"hrsh7th/nvim-cmp",
	event = "VeryLazy",
	dependencies = {
		{"hrsh7th/cmp-nvim-lsp", dependencies = { "neovim/nvim-lspconfig" }},
		{"hrsh7th/cmp-nvim-lsp-signature-help", dependencies = { "neovim/nvim-lspconfig" }},
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-path",
		"hrsh7th/cmp-nvim-lua",
		{"ray-x/cmp-treesitter", dependencies = { "nvim-treesitter/nvim-treesitter" }},
		"hrsh7th/cmp-cmdline",
		{"saadparwaiz1/cmp_luasnip" , dependencies = { "L3MON4D3/LuaSnip" }},
	},
	config = function()
		-- Setup nvim-cmp.
		local cmp = require'cmp'

		cmp.setup({
			formatting = {
				format = function(entry, vim_item)
					vim_item.menu = ({
						buffer = "[Buffer]",
						nvim_lsp = "[LSP]",
						nvim_lua = "[Lua]",
						treesitter = "[Treesitter]",
						luasnip = "[snippets]",
						path = "[Path]",
						calc = "[Calc]",
					})[entry.source.name]
						return vim_item
					end,
				},
				snippet = {
					-- REQUIRED - you must specify a snippet engine
					expand = function(args)
						require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
					end,
				},
				mapping = cmp.mapping.preset.insert({
					['<C-e>'] = cmp.mapping({
						i = cmp.mapping.abort(),
						c = cmp.mapping.close(),
					}),
					['<CR>'] = cmp.mapping.confirm(),
				}),
				sources = cmp.config.sources({
					{ name = 'treesitter', max_item_count = 10  },
					{ name = 'path', max_item_count = 10},
					{ name = 'buffer', keyword_length = 4, max_item_count = 10 },
					-- { name = 'nvim_lsp', max_item_count = 10 },
					{ name = 'lazydev', group_index = 0 },
				}),
			})

			local filetypes = {'c', 'cpp', 'ts', 'html', 'css', 'json', 'python', 'lua'} 
			for _, ft in ipairs(filetypes) do
				cmp.setup.filetype(ft, {
					mapping = cmp.mapping.preset.cmdline(),
					sources = cmp.config.sources({
						{ name = 'luasnip' },
						{ name = 'nvim_lsp_signature_help', max_item_count = 15 },
						{ name = 'nvim_lsp', max_item_count = 10 },
						{ name = 'treesitter', max_item_count = 10  },
						{ name = 'path', max_item_count = 10},
						{ name = 'buffer', keyword_length = 4, max_item_count = 10 },
					})
				})
			end

			-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
			cmp.setup.cmdline({'/', '?'}, {
				mapping = cmp.mapping.preset.cmdline(),
				sources = cmp.config.sources({
					{ name = 'buffer', keyword_length = 2 },
				})
			})

			cmp.setup.cmdline(':', {
				mapping = cmp.mapping.preset.cmdline(),
				sources = cmp.config.sources({
					{name = 'cmdline', max_item_count = 40, keyword_length = 2 },
				},
				{
					{name = 'buffer', max_item_count = 10, keyword_length = 2 },
				},
				{
					{ name = 'path' }, 
				}),
				matching = { disallow_symbol_nonprefix_matching = false }
			}
			)
		end
	},

	-- AutoPair
	{
		"windwp/nvim-autopairs",
		event = "VeryLazy",
		dependencies = {
			"hrsh7th/nvim-cmp",
		},
		config = function()
			require('nvim-autopairs').setup({
				disable_filetype = { "TelescopePrompt" },
			})

			require("cmp").setup({
				map_cr = true,
				map_complete = false,
				auto_select = false,
				map_char = {
					all = {'(','{','['},
				}
			})
		end
	},
}
