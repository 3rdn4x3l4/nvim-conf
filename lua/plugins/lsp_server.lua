return {
	{
		"folke/lazydev.nvim",
		ft = "lua", -- only load on lua files
		opts = {
		},
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{
				"williamboman/mason-lspconfig.nvim",
				dependencies = {
					"williamboman/mason.nvim"
				}
			},
			"jayp0521/mason-null-ls.nvim",
			"nvimtools/none-ls.nvim",
		},
		config = function()

			require'utils'

			vim.diagnostic.config({
				severity_sort = true,
				update_in_insert = false,
				signs = {
					-- text = {
					-- 	[vim.diagnostic.severity.ERROR] = '',
					-- 	[vim.diagnostic.severity.WARN] = '',
					-- 	[vim.diagnostic.severity.INFO] = '',
					-- 	[vim.diagnostic.severity.HINT] = '',
					-- },
					-- numhl = {
					-- 	[vim.diagnostic.severity.WARN] = 'WarningMsg',
					-- 	[vim.diagnostic.severity.ERROR] = 'ErrorMsg',
					-- 	[vim.diagnostic.severity.INFO] = 'DiagnosticInfo',
					-- 	[vim.diagnostic.severity.HINT] = 'DiagnosticHint',
					-- }
				},
				float = {
					-- source = "always"
				},
				virtual_text = false,
				underline = false,
			})

			local null_ls = require('null-ls')

			null_ls.setup({
				sources = {
					null_ls.builtins.code_actions.gitsigns,
					null_ls.builtins.completion.spell.with({
						filetypes = { "text", "NeogitCommitMessage", "markdown"},
					}),
				},
			})

			require("mason-null-ls").setup({
				automatic_installation = true,
				ensure_installed = {}
			})

			require("mason-lspconfig").setup({
				automatic_installation = true
			})

			-- Custom function attached to server
			local nvim_lsp = require'lspconfig'

			local capabilities = require('cmp_nvim_lsp').default_capabilities()
			capabilities.offsetEncoding = { "utf-16" }


			local custom_on_attach = function(client, bufnr)
				local bufopts = { noremap=true, silent=true, buffer = bufnr}

				bufopts.desc = 'rename symbol'
				vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
				bufopts.desc = 'document symbols'
				vim.keymap.set('n', '<leader>ds', require('telescope.builtin').lsp_document_symbols, bufopts)
				bufopts.desc = 'document diagnostics'
				vim.keymap.set('n', '<leader>fdg', require('telescope.builtin').diagnostics, bufopts)
				bufopts.desc = 'line diagnostics'
				vim.keymap.set('n', '<leader>dg', vim.diagnostic.open_float, bufopts)
				bufopts.desc = 'previous error'
				vim.keymap.set('n', '<leader>pd', vim.diagnostic.goto_prev, bufopts)
				bufopts.desc = 'next error'
				vim.keymap.set('n', '<leader>nd', vim.diagnostic.goto_next, bufopts)
				bufopts.desc = 'hover'
				vim.keymap.set('n', "<leader>hov", vim.lsp.buf.hover, bufopts)
				bufopts.desc = 'code actions'
				vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
				bufopts.desc = 'references'
				vim.keymap.set('n', '<leader>gr', require('telescope.builtin').lsp_references, bufopts)
				bufopts.desc = 'incoming_calls'
				vim.keymap.set('n', '<leader>gc', require('telescope.builtin').lsp_incoming_calls, bufopts)
				bufopts.desc = 'implementations'
				vim.keymap.set('n', '<leader>gi',  require('telescope.builtin').lsp_implementations, bufopts)
				bufopts.desc = 'definitions'
				vim.keymap.set('n', '<leader>gd',  require('telescope.builtin').lsp_definitions, bufopts)
				bufopts.desc = 'type definitions'
				vim.keymap.set('n', '<leader>gtd', require('telescope.builtin').lsp_type_definitions, bufopts)
				if client.supports_method("textDocument/formatting") then
					bufopts.desc = 'format'
					vim.keymap.set({ 'n', 'v' }, "<leader>fmt", function() vim.lsp.buf.format({async = true}) end, bufopts)
				end
				if client.server_capabilities.inlayHinterProvider then
					bufopts.desc =  'toggle inlay hints'
					vim.keymap.set('n', '<leader>hh', function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end, bufopts)
					vim.lsp.inlay_hint.enable(true, {bufnr = 0})
				end
			end

			-- SERVER REGISTER AND LOAD
			local servers = {
				'clangd',
				'dockerls',
				'marksman',
				'lua_ls',
				'ruff'
			}

			for _, lsp in ipairs(servers) do
				nvim_lsp[lsp].setup {
					on_attach = custom_on_attach,
					capabilities = capabilities
				}
			end

			nvim_lsp['pylsp'].setup {
				on_attach = custom_on_attach,
				capabilities = capabilities,
				settings = {
					pylsp = {
						plugins = {
							autopep8 = { enabled = false},
							flake8 = { enabled = false},
							jedi = { enabled = true},
							jedi_completion = { fuzzy = true},
							mccabe = { enabled = false},
							preload = { enabled = false},
							pycodestyle = { enabled = false},
							pyflakes = { enabled = false},
							pylint = { enabled = false},
							rope_autoimport = {
								enabled = false,
								code_actions = {
									enabled = true
								},
								completions = {
									enabled = false
								}
							},
							rope_completion = { enabled = false},
							yapf = { enabled = false},
						}
					}
				}
			}

			nvim_lsp['cssls'].setup {
				filetypes = { "css", "scss", "less" },
				settings = {
					css = {
						validate = true
					},
					less = {
						validate = true
					},
					scss = {
						validate = true
					}
				},
				on_attach = custom_on_attach,
				capabilities = capabilities
			}

			nvim_lsp['html'].setup {
				init_options = {
					-- configurationSection = { "html", "css", "javascript" },
					embeddedLanguages = {
						css = true,
						javascript = true
					}
				},
				on_attach = custom_on_attach,
				capabilities = capabilities
			}

		end
	}
}
