return {
	{
		"jay-babu/mason-nvim-dap.nvim",
		event = "VeryLazy",
		dependencies = {
			"williamboman/mason.nvim",
			"mfussenegger/nvim-dap"
		},
		config = function()
			require("mason-nvim-dap").setup({
				automatic_installation = true,
				handlers = {},
			})
		end
	},
	{
		"mfussenegger/nvim-dap",
		event = "VeryLazy",
		config = function()
			local dap = require'dap'

			local addConditionalBreakPoint = function()
				local condition = vim.fn.input('Breakpoint condition: ')
				dap.toggle_breakpoint(condition)
			end
			create_command("AddConditionalBreakPoint", addConditionalBreakPoint, {})

			-- Mappings
			local opts = {silent = true, noremap = true}
			opts.desc = 'dap continue'
			vim.keymap.set('n', '<F5>', dap.continue, opts)
			opts.desc = 'dap step over'
			vim.keymap.set('n', '<F7>', dap.step_over, opts)
			opts.desc = 'dap step into'
			vim.keymap.set('n', '<F8>', dap.step_into, opts)
			opts.desc = 'dap step out'
			vim.keymap.set('n', '<F9>', dap.step_out, opts)
			opts.desc = 'dap toggle breakpoint'
			vim.keymap.set('n', '<leader>dtb', '<cmd>AddConditionalBreakPoint<CR>', opts)
			-- Configurations + Adapters

			-- Utils get args
			local getArgs = function()
				local args_string = vim.fn.input('Arguments: ')
				return vim.split(args_string, " +")
			end

			dap.adapters.python = {
			}
			-- Python
			local venv_path = os.getenv('VIRTUAL_ENV') or os.getenv('CONDA_PREFIX')
			dap.configurations.python = {
				{
					type = 'python',
					request = 'launch',
					name = 'Launch file with arguments',
					pythonPath = venv_path
					and ((vim.fn.has('win32') == 1 and venv_path .. '/Scripts/python') or venv_path .. '/bin/python')
					or nil,
					program = '${file}',
					args = getArgs,
				},
			}

			-- C.
			dap.adapters.codelldb = {
			}

			dap.configurations.c = {
				{
					name = 'Launch with arguments',
					type = 'codelldb',
					request = 'launch',
					program = function()
						return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
					end,
					args = getArgs,
					cwd = '${workspaceFolder}',
				}
			}
			dap.configurations.cpp = dap.configurations.c
			dap.configurations.rust = dap.configurations.c
		end
	},
	{
		event = "VeryLazy",
		"theHamsta/nvim-dap-virtual-text",
		config = function()
			-- debug text
			require("nvim-dap-virtual-text").setup {
				enabled = true,                        -- enable this plugin (the default)
				enabled_commands = true,               -- create commands DapVirtualTextEnable, DapVirtualTextDisable, DapVirtualTextToggle, (DapVirtualTextForceRefresh for refreshing when debug adapter did not notify its termination)
				highlight_changed_variables = true,    -- highlight changed values with NvimDapVirtualTextChanged, else always NvimDapVirtualText
				highlight_new_as_changed = false,      -- highlight new variables in the same way as changed variables (if highlight_changed_variables)
				show_stop_reason = true,               -- show stop reason when stopped for exceptions
				commented = false,                     -- prefix virtual text with comment string
				only_first_definition = true,          -- only show virtual text at first definition (if there are multiple)
				-- all_references = true,                -- show virtual text on all all references of the variable (not only definitions)
				virt_text_win_col = 80                -- position the virtual text at a fixed window column (starting from the first text column) ,
				-- filter_references_pattern = '<module', -- filter references (not definitions) pattern when all_references is activated (Lua gmatch pattern, default filters out Python modules)
				-- experimental features:
				-- virt_text_pos = 'eol',                 -- position of virtual text, see `:h nvim_buf_set_extmark()`
				-- all_frames = false,                    -- show virtual text for all stack frames not only current. Only works for debugpy on my machine.
				-- virt_lines = false,                    -- show virtual lines instead of virtual text (will flicker!)
				--                                        -- e.g. 80 to position at column 80, see `:h nvim_buf_set_extmark()`
			}
		end
	},
	{
		event = "VeryLazy",
		"rcarriga/nvim-dap-ui",
		dependencies = {
			"mfussenegger/nvim-dap",
			"nvim-neotest/nvim-nio"
		},
		config = function()
			local dapui = require("dapui")
			-- Mappings
			local opts = {silent = true, noremap = true}
			opts.desc = 'toogle dap ui'
			vim.keymap.set('n', '<F4>', dapui.toggle, opts)
			dapui.setup({
				layouts = {
					{
						elements = {
							"stacks",
							"scopes",
							"breakpoints",
						},
						size = 0.20,
						position = "left",
					},
					{
						elements = {
							"repl",
							"watches",
						},
						size = 0.33, -- 25% of total lines
						position = "right",
					},
				},
			})


		end
	},
}
