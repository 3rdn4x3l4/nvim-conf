return {
	-- Git
	{
		event = "VeryLazy",
		"NeogitOrg/neogit",
		dependencies = {
			"sindrets/diffview.nvim",
			"nvim-telescope/telescope.nvim",
			"ellisonleao/gruvbox.nvim"
		},
		config = function()

			require('neogit').setup({
				disable_signs = false,
				disable_hint = true,
				disable_context_highlighting = false,
				disable_commit_confirmation = true,
				use_telescope = true,
				-- Neogit refreshes its internal state after specific events, which can be expensive depending on the repository size. 
				-- Disabling `auto_refresh` will make it so you have to manually refresh the status after you open it.
				auto_refresh = false,
				disable_builtin_notifications = false,
				use_magit_keybindings = false,
				-- Change the default way of opening neogit
				kind = "tab",
				-- Change the default way of opening the commit popup
				commit_popup = {
					kind = "split",
				},
				-- Change the default way of opening popups
				popup = {
					kind = "split",
				},
				-- customize displayed signs
				signs = {
					-- { CLOSED, OPENED }
					section = { ">", "v" },
					item = { ">>", "vv" },
					hunk = { "", "" },
				},
				integrations = {
					diffview = true,
					telescope = true
				},
			})

			vim.keymap.set('n', '<leader>gss', function() require("neogit").open({cwd = getPathOfCurrentFile()}) end, { silent = true, noremap = true, desc = 'open neogit'})
		end
	},

	{
		event = "VeryLazy",
		"lewis6991/gitsigns.nvim",
		opts = {
			signs = {
				add = { text = '+'},
				delete = { text = '-'}
			},
			on_attach = function(bufnr)
				local gitsigns = require("gitsigns")
				local opts = { silent = true, noremap = true, buffer = 0}

				vim.keymap.set('n', '<leader>nh', function() gitsigns.nav_hunk("next") end, opts)
				vim.keymap.set('n', '<leader>ph', function() gitsigns.nav_hunk('prev') end, opts)
				vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, opts)
			end,
			current_line_blame_formatter = '<abbrev_sha>| <author>, <author_time:%R> - <summary>',
		}
	},

	{
		event = "VeryLazy",
		"sindrets/diffview.nvim",
		dependencies = {"nvim-tree/nvim-web-devicons","nvim-lua/plenary.nvim"},

		-- some comment
		opts = {
			view = {
				-- Configure the layout and behavior of different types of views.
				-- Available layouts:
				--  'diff1_plain'
				--    |'diff2_horizontal'
				--    |'diff2_vertical'
				--    |'diff3_horizontal'
				--    |'diff3_vertical'
				--    |'diff3_mixed'
				--    |'diff4_mixed'
				-- For more info, see |diffview-config-view.x.layout|.
				default = {
					-- Config for changed files, and staged files in diff views.
					layout = "diff2_vertical",
					disable_diagnostics = false,  -- Temporarily disable diagnostics for diff buffers while in the view.
					winbar_info = false,          -- See |diffview-config-view.x.winbar_info|
				},
			},
		}
	},

}
