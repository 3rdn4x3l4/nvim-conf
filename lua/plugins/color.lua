return  {
	{
		"ellisonleao/gruvbox.nvim",
		config = function()
			local gruvbox = require("gruvbox")
			local colors = gruvbox.palette

			local borderColors = { bg = colors.dark0, fg = colors.dark0 }
			local titleColors = { bg = colors.dark0, fg = colors.gray }
			local normalColors = { bg = colors.dark0, fg = colors.light1 }
			local invertedBorderColors = { fg = colors.dark0, bg = colors.dark0 }
			local invertedNormalColors = { fg = colors.dark0, bg = colors.light1 }

			local overrides = {
				TelescopeNormal = { fg = colors.gray },

				TelescopePreviewBorder = borderColors,
				TelescopePreviewNormal = normalColors,
				-- TelescopePreviewTitle = titleColors,
				TelescopePreviewTitle = borderColors,

				TelescopePromptBorder = invertedBorderColors,
				TelescopePromptNormal = invertedNormalColors,
				-- TelescopePromptTitle = titleColors,
				TelescopePromptTitle = borderColors,
				TelescopePromptPrefix = invertedNormalColors,

				TelescopeResultsBorder = borderColors,
				TelescopeResultsNormal = normalColors,
				-- TelescopeResultsTitle = titleColors,
				TelescopeResultsTitle = borderColors,
			}

			local options = {
				terminal_colors = true,
				undercurl = true,
				underline = true,
				bold = true,
				italic = {
					strings = false,
					comments = false,
					operators = false,
					folds = false,
				},
				strikethrough = true,
				invert_selection = true,
				invert_signs = false,
				invert_tabline = false,
				invert_intend_guides = false,
				inverse = true, -- invert background for search, diffs, statuslines and errors
				contrast = "soft", -- can be "hard", "soft" or empty string
				dim_inactive = false,
				transparent_mode = false,
				overrides = overrides
			}

			-- setup must be called before loading the colorscheme
			gruvbox.setup(options)
		end
	},
	--Colors
	{ 
		"norcalli/nvim-colorizer.lua",
		opts = {
			'css',
			'javascript',
			'html',
			'lua',
		}
	},

	{ 
		"lukas-reineke/indent-blankline.nvim",
		main = "ibl",
		config = function()
			vim.cmd [[highlight IndentBlanklineIndent1 guibg=#282828 gui=nocombine]]
			vim.cmd [[highlight IndentBlanklineIndent2 guibg=#3c3836 gui=nocombine]]

			local opts = {
				enabled = true,
				indent = {
					char = "",
					highlight = {
						"IndentBlanklineIndent1",
						"IndentBlanklineIndent2",
					},
					smart_indent_cap = false,
				},
				whitespace = {
					highlight = {
						"IndentBlanklineIndent1",
						"IndentBlanklineIndent2",
					},
					remove_blankline_trail = true,
				},
				scope = {
					enabled = true,
					show_start = true,
					show_end = true,
					injected_languages = false,
					highlight = {
						"IndentBlanklineIndent1",
						"IndentBlanklineIndent2",
					},
					exclude = { language = { "c" } }
				}
			}
			require('ibl').setup(opts)
		end
	},

}
