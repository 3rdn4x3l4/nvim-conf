- [x] make nvim config a submodule of dotfiles
- [x] add nvim config to 42-nvimevaluator
- [ ] refactor plugins to be inside a lazy.nvim table 
```lua
plugin-config = {
    pluginurl = "...",
    config = function()
        -- my plugin config
    end
}
```
