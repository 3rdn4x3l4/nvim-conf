-- require dirs inside lua dir
require("settings")
require("utils")

-- Bootstrap
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable", -- latest stable release
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins")

vim.cmd([[colorscheme gruvbox]])
vim.keymap.set('n', 'n', 'nzz', { silent = true, noremap = true, desc = 'center on jump' })
vim.keymap.set('n', 'N', 'Nzz', { silent = true, noremap = true, desc = 'center on jump' })
vim.keymap.set('n', 'H', 'Hzz', { silent = true, noremap = true, desc = 'center on jump' })
vim.keymap.set('n', 'M', 'Mzz', { silent = true, noremap = true, desc = 'center on jump' })
vim.keymap.set('n', 'L', 'Lzz', { silent = true, noremap = true, desc = 'center on jump' })

vim.keymap.set('n', '<C-d>', '<C-d>zz', { silent = true, noremap = true, desc = 'center on jump' })
vim.keymap.set('n', '<C-u>', '<C-u>zz', { silent = true, noremap = true, desc = 'center on jump' })

vim.keymap.set('n', '<Esc>', '<Esc>:nohlsearch<CR>', { silent = true, noremap = true, desc = 'remove search highlight' })

vim.keymap.set('n', '<leader>cb', '<cmd>lcd %:p:h<CR>', { silent = true, noremap = true, desc = 'lcd to buffer' })

vim.keymap.set('n', '<leader>qq', '<cmd>qa<CR>', { silent = true, noremap = true, desc = 'full quit' })

vim.keymap.set('n', '<leader><leader>x', '<cmd>source %<CR>', { silent = true, noremap = true, desc = 'execute file' })
vim.keymap.set('n', '<leader>x', ':.lua<CR>', { silent = true, noremap = true, desc = 'source line' })
vim.keymap.set('v', '<leader>x', ':lua<CR>', { silent = true, noremap = true, desc = 'source visual selection' })
vim.api.nvim_create_autocmd({ "FileType" }, {
	pattern = { "help" },
	callback =  function(opts)
		vim.keymap.set("n", "gd", "<C-]>", { silent = true, buffer = opts.buf })
	end
})
